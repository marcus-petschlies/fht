#!/bin/bash
MyName=$(echo $0 | awk -F\/ '{sub(/\.sh/,"",$NF);print $NF}')

echo "# [$MyName] (`date`)"

top_level_dir=$PWD

out=$MyName.out
err=$MyName.err

bindir=/home/marcuspe/software/cvc/cpff/build-ubuntu18-serial-openmp-nowrapper

P2CUT=4    # (2pi/L)^2
P4CUT=-1   # (2pi/L)^2
P6CUT=-1   # (2pi/L)^2
Q2CUT=0.2  # GeV^2

ENSNAME="cB211.072.64"

# valgrind -v --leak-check=full --show-reachable=yes \

# $bindir/get_q2_list -N "cA211.53.24" -C 100,-1,-1 -q 1.  1>out 2>err

$bindir/get_q2_list -N $ENSNAME -C ${P2CUT},${P4CUT},${P6CUT} -q $Q2CUT  1>$out 2>$err

echo "# [$MyName] exit status was $?"

echo "# [$MyName] (`date`)"

exit 0
