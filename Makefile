main = main
out  = $(main)

all: pdfnb

pdf:
	pdflatex ${main}
	pdflatex ${main}
	bibtex ${main}
	bibtex ${main}
	pdflatex ${main}
	pdflatex ${main}

pdfnb:
	pdflatex ${main}
	pdflatex ${main}

clean:
	rm -f *~ *.log *.toc *.bbl *.blg *.lof *.lot *.aux *.out
