\section{Trace over states}

On the finite Euclidean lattice with periodicity in time, we get contributions form any ordering of state interpolators and 
operator insertion. What contributions are dominant, depends on the suppression by time exponentials.
\begin{center}
  \includegraphics[width=0.4\textwidth]{./images/lattice_clock.pdf}
\end{center}

As usual we have the lattice with time extent $T$ and we call the transfer matrix $\Ttransfer$ 
with 
\begin{align}
  \Ttransfer &= \exp\left( -a \Hhamiltonian \right)
  \label{eq:T_and_H}
\end{align}
in terms of Hamiltonain $\Hhamiltonian$.

\subsection{Euclidean time ordering ``final -- current -- initial''}

Let us first do the exercise for $t_f \ge t_c \ge t_i$.  What we calculate is

\begin{align}
  & \brackets{
    J_{\pi_f}(t_f)\; O_S(t_c)\; J_{\pi_i}(t_i)^\dagger
  }
  \label{eq:3pt_1} \\
  %%%
  &= \Tr{ \Ttransfer^{T-t_f} \; J_{\pi_f}(t_f)\; \Ttransfer^{t_f-t_c}\; O_S(t_c)\; \Ttransfer^{t_c - t_i}\; J_{\pi_i}(t_i)^{\dagger}\;
  \Ttransfer^{t_i}
  }
  \nonumber
\end{align}
We focus on scalar ($S$) and pseudoscalar ($P$) states for now; and we have the following non-zero overlaps
\begin{align}
  \brackets{S\;|\;J_{\pi}\;|\;P } &\ne 0 \ne \brackets{P\;|\;J_{\pi}^\dagger\;|\; S}
  \label{eq:ov_Jpi} \\
  %%%
  \brackets{S\;|\;O_S\;|\;S } &\ne 0 \ne \brackets{P\;|\;O_S\;|\; P}
  \label{eq:ov_OS}
\end{align}

Among the $S$ states is the vacuum $\Omega$ and among the $P$ states the $\pi$.\\

Then we get the following contributions
\begin{align}
  & \Tr{ \Ttransfer^{T-t_f} \; J_{\pi_f}(t_f)\; \Ttransfer^{t_f-t_c}\; O_S(t_c)\; \Ttransfer^{t_c - t_i}\; J_{\pi_i}(t_i)^{\dagger}\;
  \Ttransfer^{t_i}
} =  \nonumber \\ 
  %%%
& + \quad \sum\limits_{S,P_1,P_2}\;
   \brackets{S\;|\;J_{\pi_f}\;|\;P_1}\;\brackets{P_1\;|\;O_S\;|\;P_2}\;\brackets{P_2\;|\;J_{\pi_i}^\dagger\;|\;S} \;
   \epow{-E_{S}(T-t_f + t_i)} \; \epow{-E_{P_1}(t_f-t_c)}\;\epow{-E_{P_2}(t_c-t_i)}
  \label{eq:contrib_SPP}  \\
  %%%
&  +\quad \sum\limits_{S_1,S_2,P}\;
   \brackets{P\;|\;J_{\pi_f}\;|\;S_1}\;\brackets{S_1\;|\;O_S\;|\;S_2}\;\brackets{S_2\;|\;J_{\pi_i}^\dagger\;|\;P} \;
   \epow{-E_{P}(T-t_f + t_i)} \; \epow{-E_{S_1}(t_f-t_c)}\;\epow{-E_{S_2}(t_c-t_i)}
  \label{eq:contrib_SSP}
  %%%
\end{align}

Notable contributions from eqs. \refeq{eq:contrib_SPP} and \refeq{eq:contrib_SSP}

\paragraph{The wanted signal ( forward contribution )}

$S = \Omega$, $P_1 = \pi = P_2$, $E_{\Omega} = 0$
\begin{align}
  \brackets{\Omega\;|\;J_{\pi_f}\;|\;\pi}\;\brackets{\pi\;|\;O_S\;|\;\pi}\;\brackets{\pi\;|\;J_{\pi_i}^\dagger\;|\;\Omega} \;
   \epow{-E_{\pi_f}(t_f-t_c)}\;\epow{-E_{\pi_i}(t_c-t_i)}
  \label{eq:wanted_signal}
\end{align}

\paragraph{The vacuum expectation ( backward contribution ) }

$S_1 = \Omega = S_2$, $P = \pi$

\begin{align}
   \brackets{\pi\;|\;J_{\pi_f}\;|\;\Omega}\;\brackets{\Omega\;|\;O_S\;|\;\Omega}\;\brackets{\Omega\;|\;J_{\pi_i}^\dagger\;|\;\pi} \;
   \epow{-E_{\pi_f}(T-t_f)} \; \epow{-E_{\pi_i} t_i}
  \label{eq:vacuum_contribution}
\end{align}

\textbf{Note:} needs to be subtracted. \\

\paragraph{1st excited scalar contamination }

$S_1 = S = S$, $P = \pi$
\begin{align}
  \brackets{\pi\;|\;J_{\pi_f}\;|\;S} \; \brackets{S\;|\;O_S\;|\;\Omega}\;\brackets{\Omega\;|\;J_{\pi_i}^\dagger\;|\;\pi} \;
   \epow{-E_{\pi_f}(T-t_f)} \; \epow{-E_{S}(t_f-t_c)}\;\epow{-E_{\pi_i} t_i}
   \nonumber\\
   %%%
   + \brackets{\pi\;|\;J_{\pi_f}\;|\;\Omega} \; \brackets{\Omega\;|\;O_S\;|\;S}\;\brackets{S\;|\;J_{\pi_i}^\dagger\;|\;\pi} \;
   \epow{-E_{\pi_f}(T-t_f)} \; \epow{-E_{S}(t_c-t_i)}\;\epow{-E_{\pi_i} t_i}
  \label{eq:1st_excited_scalar}
\end{align}

All things being relative and equal, eq. \refeq{eq:1st_excited_scalar} can also be seen as a contribution from the
flavor-changing, pseudoscalar form factor $\brackets{\pi\;|\;J_{\pi_f}\;|\;S}$.

\subsection{Euclidean time ordering ``current -- final -- initial''}

Same exercise for $t_c \ge t_f \ge t_i$; we get
\begin{align}
  & \brackets{
    J_{\pi_f}(t_f)\; O_S(t_c)\; J_{\pi_i}(t_i)^\dagger
  } =
  \label{eq:3pt_cfi} \\
  %%%
  %%%
  %%%
& + \quad \sum\limits_{S,P_1,P_2}\;
   \brackets{P_1\;|\;O_S\;|\;P_2} \;
   \brackets{P_2\;|\;J_{\pi_f}\;|\;S} \;
   \brackets{S\;|\;J_{\pi_i}^\dagger\;|\;P_1} \;
   %%%
   \epow{-E_{P_1}(T-t_c + t_i)} \; \epow{-E_{P_2}(t_c-t_f)}\;\epow{-E_{S}(t_f-t_i)}
  \label{eq:contrib_SPP_cfi}  \\
  %%%
&  +\quad \sum\limits_{S_1,S_2,P}\;
   \brackets{S_1\;|\;O_S\;|\;S_2} \;
   \brackets{S_2\;|\;J_{\pi_f}\;|\;P} \;
   \brackets{P\;|\;J_{\pi_i}^\dagger\;|\;S_1} \;
   %%%
   \epow{-E_{S_1}(T-t_c+t_i)} \; \epow{-E_{S_2}(t_c-t_f)}\;\epow{-E_{P}(t_f-t_i)}\,,
  \label{eq:contrib_SSP_cfi}
\end{align}
which amounts to the identification $t_i \leftrightarrow T + t_i$.
If needed, we can again single out the dominant contributions. 

%\paragraph{When is it relevant ? } 
%
%
%With finite $T$ we can arrange the trace and time sequences in whichever order we want. \textbf{As soon as} $T$ or $T - ( t_f - t_i) $ is not large enough
%to suppress backward running states. Especially for the pion, where we know, that backward is relevant ($\rightarrow \cosh$).\\


%What is the mass of the smallest scalar excitation ?

\subsection{Special case of pion at source and sink with zero momentum transfer}
As e.g. relevant for $\brackets{x}$ and scalar FF.

The contributions from the two time orderings give for $P = P_1 = \pi = P_2$, $S = S_1 = \Omega = S_2$ in the formulas above
\begin{align}
  \brackets{
    J_{\pi_f}(t_f)\; O(t_c)\; J_{\pi_i}(t_i)^\dagger
  }_{f-c-i} &= 
  Z_\pi^2 \,\matelem_{\pi}\;\epow{-E_\pi \Delta t} + 
  Z_\pi^2 \,\matelem_{\Omega}\;\epow{-E_\pi (T - \Delta t)} 
  \label{eq:pi_J_pi_fci} \\
  %%%
  \brackets{ 
    J_{\pi_f}(t_f)\; O(t_c)\; J_{\pi_i}(t_i)^\dagger
  }_{c-f-i} &= 
  Z_\pi^2 \,\matelem_{\pi}\;\epow{-E_\pi ( T - \Delta t)} + 
  Z_\pi^2 \,\matelem_{\Omega}\;\epow{-E_\pi \Delta t} 
  \label{eq:pi_J_pi_cfi}
\end{align}
Here $\Delta t = t_f - t_i$, $\matelem_X = \brackets{X\;|\;O\;|\;X}$.\\

%By \textbf{folding} the 3-point function, i.e. summation of the signal for $\Delta$

\subsection{Subtraction of vacuum term}

The full correlation function we consider contains the subtraction of the vacuum term ``loop x 2-point''. Again restricting to
pion and vacuum states, we would get for final-current-initial time ordering
\begin{align}
 & \brackets{
    J_{\pi_f}(t_f)\; O(t_c)\; J_{\pi_i}(t_i)^\dagger
  } - 
  \brackets{O(t_c)} \;
  \brackets{
    J_{\pi_f}(t_f)\; J_{\pi_i}(t_i)^\dagger
  }
  \label{eq:3pt_sub} \\
  %%%
  &=   Z_\pi^2 \,\matelem_{\pi}\;\epow{-E_\pi \Delta t} +
  Z_\pi^2 \,\matelem_{\Omega}\;\epow{-E_\pi (T - \Delta t)} - Z_\pi^2\, \matelem_{\Omega}\;\left( \epow{-E_\pi (t_f - t_i)} + \epow{-E_\pi (T - t_f + t_i)} \right)
  \nonumber\\
  %%%
  &= Z_{\pi}^2\;\left( \matelem_{\pi} - \matelem_{\Omega} \right)\;\epow{-E_{\pi} (t_f - t_i)}
  \nonumber
\end{align}
Analogously for the alternate time ordering. Thus the backward vev term is canceled, only the forward parts remain.\\

(\textbf{Question:} Subtraction in all cases ? Also for the moments ?)

%\subsection{Folding the 3-point function}

%What we'll want to do is fold the 3-point function analogously to the 2-point function.
%, i.e. for a given 
%$\Delta t_{fc} = t_f - t_c $ and $\Delta t_{ci} = t_c - t_i$ we combine
%\begin{align}
% & \brackets{J_{\pi_f}(t_f)\; O_S(t_c)\; J_{\pi_i}(t_i)^\dagger } + 
%  \brackets{J_{\pi_i}(t_i+T)^\dagger\; O_S(t_c)\; J_{\pi_f}(t_f) } 
%  \nonumber \\
%  %%%
%  & \quad = C_{3pt}(t_f-t_c, t_c-t_i ) + C_{3pt}(t_i+T-t_c, t_c-t_f )
%  \nonumber \\
%  %%%
%  & \quad = C_{3pt}( \Delta t_{fc}, \Delta t_{ci} ) + C_{3pt}( T - \Delta t_{ci}, -\Delta t_{fc} )
%  \label{eq:folding_1}
%\end{align}

%\subsection{Folding the 3-point function}

%What we'll want to do is fold the 3-point function analogously to the 2-point function.
%, i.e. for a given 
%$\Delta t_{fc} = t_f - t_c $ and $\Delta t_{ci} = t_c - t_i$ we combine
%\begin{align}
% & \brackets{J_{\pi_f}(t_f)\; O_S(t_c)\; J_{\pi_i}(t_i)^\dagger } + 
%  \brackets{J_{\pi_i}(t_i+T)^\dagger\; O_S(t_c)\; J_{\pi_f}(t_f) } 
%  \nonumber \\
%  %%%
%  & \quad = C_{3pt}(t_f-t_c, t_c-t_i ) + C_{3pt}(t_i+T-t_c, t_c-t_f )
%  \nonumber \\
%  %%%
%  & \quad = C_{3pt}( \Delta t_{fc}, \Delta t_{ci} ) + C_{3pt}( T - \Delta t_{ci}, -\Delta t_{fc} )
%  \label{eq:folding_1}
%\end{align}

